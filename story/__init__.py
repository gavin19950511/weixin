import requests
import json

#
VOISS_KEY = '2vxcjoidf44he21d4c2xgr'
VOISS_API_TALK = f'https://api.voiss.cc/wc-story/conv?key={VOISS_KEY}'
VOISS_API_RESET = f'https://api.voiss.cc/wc-story/conv/reset?key={VOISS_KEY}'
SOTRY_ID = "b-horse"

#
def get_reply(text,userid):
    r = requests.post(VOISS_API_TALK,
                      data=json.dumps({
                            "s": userid,
                            "query": text,
                            "storyId": SOTRY_ID
                      }),
                      headers={"Content-Type": "application/json"})
    result = r.json()
    return [r['answer']for r in result['responses']]

def reset_story(userid):
    if userid is None:
        return []
    r = requests.post(VOISS_API_RESET,
                      data=json.dumps({
                            "s": userid
                      }),
                      headers={"Content-Type": "application/json"})
    result = r.json()
    return ["reset:"+result['sid']]


#
def switch(text,**kwargs) -> list:

    if text == "RESET":
        return reset_story(kwargs.get("user_id"))
    else:
        return get_reply(text,kwargs.get("user_id"))
