import datetime
import json


from weixin_tool.WXBizMsgCrypt import Prpcrypt, WXBizMsgCrypt
from weixin_tool.sendmsg import send
from weixin_tool.convertor import xml2json, json2xml

startTime = datetime.datetime.now().strftime("%Y-%b-%d %H:%M:%S")


class TextMsg:
    def __init__(self, json_obj):
        self.json_obj:dict = json_obj

    def __str__(self):
        return json.dumps(self.json_obj)

    def check_msg_from_client(self):
        return self.json_obj.get("from") == "0"

    @staticmethod
    def from_xml(xml_str):
        return TextMsg(xml2json(xml_str))

    def to_xml(self):
        return json2xml(self.json_obj)

    def to_json(self):
        return self.json_obj

    def get(self, k, default=None):
        return self.json_obj.get(k, default)


class InterFace:
    def __init__(self, key, appid, token):
        self.encoding_aes_key = key
        self.decrypter = Prpcrypt(self.encoding_aes_key)
        self.appid = appid
        self.token = token
        self.encryptor = WXBizMsgCrypt(self.token, self.encoding_aes_key, self.appid)

    def send(self, user_id, msg):
        return send(self.appid, user_id, msg, self.token, self.encryptor)

    def decrypt(self, crypted_str):
        status, t = self.decrypter.decrypt(crypted_str, self.appid)
        return status, TextMsg.from_xml(t)


#   --  func  --  #

def gen_pct(key):
    return Prpcrypt(key)

