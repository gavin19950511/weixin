from weixin_tool.convertor import json2xml, xml2json
from weixin_tool.WXBizMsgCrypt import WXBizMsgCrypt
import json
import requests


NONCE = "1609212783695"


def send(app_id, openid, msg, weixin_openai_token, cm: WXBizMsgCrypt):
    obj = gen_msg(app_id, openid, msg, cm)
    # 將資料加入 POST 請求中
    r = requests.post(f'https://openai.weixin.qq.com/openapi/sendmsg/{weixin_openai_token}',
                      data=json.dumps(obj),
                      headers={"Content-Type": "application/json"})
    return r


def gen_msg(app_id, openid, msg, cm):
    payload = {"xml": {
        "appid": f"<![CDATA[{app_id}]]>",
        "openid": f"<![CDATA[{openid}]]>",
        "msg": f"<![CDATA[{msg}]]>",
        "channel": 7
    }}
    data = json2xml(payload)
    status, re = cm.EncryptMsg(sReplyMsg=data, sNonce=NONCE)
    obj = xml2json(re)
    obj['encrypt'] = obj['Encrypt']
    return obj
