from flask import Flask
from flask import request
from weixin_tool import  InterFace
from story import switch

weixin_openai_token = "bI3M2VKv5fdfHcSK9Fd0IuPCpxmNiD"
weixin_openai_appid = "lKsj5q57p4AVllw"
weixin_openai_EncodingAESKey = "hGJdYZmRgd9tzxDOnbaQDuWzcRApAIG1n7ybgcdDX3y"

#
weixin_helper = InterFace(key=weixin_openai_EncodingAESKey, appid=weixin_openai_appid, token=weixin_openai_token)
app = Flask(__name__)


@app.route("/", methods=['GET', "POST"])
def test():
    return {
        "status": True
    }


# 對話開放平台
@app.route("/demo2", methods=['POST'])
def hook_talk():
    data = request.get_json()
    # print("hook_talk data", data)
    status, result = weixin_helper.decrypt(crypted_str=data['encrypted'])
    if result.check_msg_from_client():
        #
        user_talk_text = result.get("content", {}).get("msg")
        user_id = result.get("userid")
        #
        answers = switch(text=user_talk_text, user_id=user_id)

        #
        for a in answers:
            weixin_helper.send(user_id, a)
    else:
        print(f"skip: {result.get('from')}")
    return {}


# 公眾平台
# @app.route("/demo", methods=['GET', 'POST'])
# def hook():
#     if request.method == 'GET':
#         print("data", request.data)
#         params = dict(request.values)
#         # combine = "".join([v for v in params.values()])
#         # sha1 = hashlib.sha1(combine.encode('utf-8')).hexdigest()
#         # print(sha1)
#         print({"echostr":params['echostr']})
#
#         return params['echostr']
#     else:
#         print(request.data)
#         a = xml2json(request.data)
#         """
#         {'ToUserName': 'gh_b7c8dfcd59af', 'FromUserName': 'oKHAb5nfDSbZQqRe-RhP3YhJOOpk',
#         'CreateTime': '1609148178', 'MsgType': 'text', 'Content': '看看', 'MsgId': '23037465097519542'}
#         """
#         re = json2xml({
#             "xml": {
#                 "ToUserName": a['FromUserName'],
#                 "FromUserName": a['ToUserName'],
#                 "CreateTime": a['CreateTime'],
#                 "MsgType": a['MsgType'],
#                 "Content": a['Content'],
#             }
#         })
#
#         pct = gen_pct("hGJdYZmRgd9tzxDOnbaQDuWzcRApAIG1n7ybgcdDX3y")
#
#         data = json2xml({
#             "appid": a["appid"],
#             "openid": a["userid"],
#             "msg": a["content"]['msg'],
#             "channel": a['channel']
#         })
#         return pct.encrypt(data)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
