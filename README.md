# weixin

Demo on Flask


#### run
```
    pip install -r requirements.txt
    python app.py
```

#### run on docker-compose
```
    docker-compose build
    docker-compose up -d
```

#### story
```
    # ~/weixin/story
    connect to VOISS api
```

#### weixin_tool
```
    # ~/weixin/weixin_tool
    interface for weixin
```